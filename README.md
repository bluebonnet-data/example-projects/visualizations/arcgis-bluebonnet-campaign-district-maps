# ArcGis Bluebonnet Campaign District Maps

Identifying what campaigns Bluebonnet is a part of 
# Links to ArcGIS Maps 
https://www.arcgis.com/home/webmap/viewer.html?webmap=5b74e6a449f94d79bbc9686cdb1cb81d
# Documentation 

## Starting with the tutorial 

Clark County Maps 

## Layers 

* You can add layers by searching online or importing csv's or shape layers 
* Download layers you find online 


## Filters 
* Filter -- go to layers, go to filter icon, go to Edit, select the dropdown choice for Match Any of the Expressions below, and then add the districts that Bluebonnet is following
